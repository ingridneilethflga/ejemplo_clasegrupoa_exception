package Vista;

import Modelo.Persona;
import java.util.Scanner;

/**
 *
 * @author BunnyMoon
 */
public class Test_PersonaException {

    public static void main(String[] args) {

        Persona p1 = crearPersona();
        System.out.println(p1.toString());
    }

    private static Persona crearPersona() {
        Persona p1;//instanciando un objeto de la clase persona
        System.out.println("-------------------- Creando Persona ----------------\n");
        try {
            p1 = new Persona((short) leerShort("Digite el Dia: "), (short) leerShort("Digite el mes: "), (short) leerShort("Digite el año: "), (short) leerShort("Digite la hora: "), (short) leerShort("Digite el Minuto: "), (short) leerShort("Digite el segundo: "));
            return p1;
        } catch (Exception ex) {
            System.out.println("Error:" + ex.getMessage());
            return crearPersona();
        }
    }

    private static short leerShort(String msg) {
        System.out.print(msg);
        Scanner in = new Scanner(System.in);
        try {
            return in.nextShort();
        } catch (java.util.InputMismatchException ex) {
            System.err.println("Error no es un short");
            return leerShort(msg);
        }
    }
}
