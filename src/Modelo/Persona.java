
package Modelo;

/**
 *  Clase representa una persona con su fecha de nacimiento
 * @author Ingrid Florez
 */
public class Persona {
    
    private String nombresCompletos;
    //Su fecha de nacimiento
    private short dia, mes,agno, hora, min, seg;
    
    //Validar que una fecha EXISTA

    public Persona() {
    }
    
    public Persona( short dia, short mes, short agno, short hora, short min, short seg) throws Exception  {
    this.dia = dia;
    this.mes = mes;
    this.agno = agno;
    this.hora = hora;
    this.min = min;
    this.seg = seg;

    this.validarFecha();
    }
        
    private void validarFecha()throws Exception {
     if (this.agno <= 0 || this.mes <= 0 || this.dia <= 0 || this.hora < 0 || this.min < 0 || this.seg < 0) {
         throw new Exception("Fecha invalida: " + this.dia + "/" + this.mes + "/" + this.agno + " "+ this.hora + ":" + this.min + ":" + this.seg +" La fecha no puede ser 0 o menor");
     }else if (this.mes > 12){
         throw new Exception("Fecha invalida: " + this.dia + "/" + this.mes + "/" + this.agno + " El mes no puede ser mayor a 12");
     }else if (this.mes == 2 && this.dia > 28){
         throw new Exception("Fecha invalida, El mes: " + this.mes + " no puede tener mas de 28 dias");
     }else if (this.mes == 4 || this.mes == 6 || this.mes == 9 || this.mes == 11 && this.dia > 30){
         throw new Exception("Fecha invalida, El mes: " + this.mes + " no puede tener mas de 30 dias");
     }else if(this.dia > 31){
         throw new Exception("Fecha invalida, El mes: " + this.mes + " no puede tener mas de 31 dias");
     }else if (this.hora > 23 || this.min > 59 || this.seg > 59){
         throw new Exception("Hora invalida: " + this.hora + ":" + this.min + ":" + this.seg + " La hora es incorrecta");
     }        
    }  

    public short getDia() {
        return dia;
    }

    public short getMes() {
        return mes;
    }

    public short getAgno() {
        return agno;
    }

    public short getHora() {
        return hora;
    }

    public short getMin() {
        return min;
    }

    public short getSeg() {
        return seg;
    }

    public void setDia(short dia) {
        this.dia = dia;
    }

    public void setMes(short mes) {
        this.mes = mes;
    }

    public void setAgno(short agno) {
        this.agno = agno;
    }

    public void setHora(short hora) {
        this.hora = hora;
    }

    public void setMin(short min) {
        this.min = min;
    }

    public void setSeg(short seg) {
        this.seg = seg;
    }

    @Override
    public String toString() {
        return "Nació el dia= " + dia + " del mes= " + mes + " del año= " + agno + " a las " + hora + " horas " + min + " minutos y " + seg + " segundos";
    }
    
    
}
